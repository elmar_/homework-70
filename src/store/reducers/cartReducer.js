import {ADD_DISH, CLIENT_ERROR, CLIENT_REQUEST, CLIENT_SUCCESS, FIRST_ADD, REMOVE_DISH} from "../actions/cartActions";

const initialState = {
    delivery: 150,
    dishes: {},
    totalPrice: 0,
    loading: false
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case FIRST_ADD:
            return {...state, dishes: action.dish, totalPrice: state.totalPrice + state.delivery}
        case ADD_DISH:
            return {
                ...state,
                dishes: {
                    ...state.dishes,
                    [action.id]: {
                        ...state.dishes[action.id],
                        amount: state.dishes[action.id].amount + 1
                    }
                },
                totalPrice: state.totalPrice + state.dishes[action.id].price
            };
        case REMOVE_DISH:
            return {
                ...state,
                dishes: {
                    ...state.dishes,
                    [action.id]: {
                        ...state.dishes[action.id],
                        amount: state.dishes[action.id].amount - 1
                    },
                },
                totalPrice: state.totalPrice - state.dishes[action.id].price
            };
        case CLIENT_REQUEST:
            return {...state, loading: true};
        case CLIENT_SUCCESS:
            return {...state, loading: false};
        case CLIENT_ERROR:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default cartReducer;