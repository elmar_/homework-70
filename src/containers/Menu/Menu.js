import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes} from "../../store/actions/menuActions";
import Dish from "../../components/Dish/Dish";

import './Menu.css';
import {addDish} from "../../store/actions/cartActions";
import Spinner from "../../components/Spinner/Spinner";


const Menu = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.menu.dishes);
    const loading = useSelector(state => state.menu.loading);

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);

    const addToCart = id => {
        dispatch(addDish(id));
    };

    let menu = dishes.map(dish => (
        <Dish key={dish.dish} dishData={dish} addToCart={addToCart} />
    ));

    if (loading) {
        menu = <Spinner />;
    }


    return (
        <div className="Menu">
            <h3 className="title">Menu</h3>
            {menu}
        </div>
    );
};

export default Menu;