import axios from "axios";

const axiosFetch = axios.create({
    baseURL: 'https://classwork-63-burger-default-rtdb.firebaseio.com/homework-70/'
});

export default axiosFetch;