import React from 'react';
import Cart from "./containers/Cart/Cart";
import Menu from "./containers/Menu/Menu";
import ClientData from "./containers/ClientData/ClientData";
import './App.css';

const App = () => {
    return (
        <div className="out">
            <div className="container">
                <Menu />
                <Cart />
            </div>
            <ClientData />
        </div>
    );
};

export default App;