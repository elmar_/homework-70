import React, {useState} from 'react';
import './ClientData.css';
import Backdrop from "../../components/Backdrop/Backdrop";
import {useDispatch, useSelector} from "react-redux";
import {postClient} from "../../store/actions/cartActions";
import Spinner from "../../components/Spinner/Spinner";

const ClientData = ({show, close}) => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.cart.dishes);
    const loading = useSelector(state => state.cart.loading);

    const [client, setClient] = useState({
        name: '',
        phone: '',
        street: ''
    });

    const changeInput = e => {
        const {name, value} = e.target;

        setClient({
            ...client,
            [name]: value
        });
    };


    const sendData = () => {
        const orderToSend = [];
        for (const order in orders) {
            orderToSend.push({...orders[order]})
        }

        for (let i = 0; i < orderToSend.length; i++) {
            delete orderToSend[i]['price'];
            delete orderToSend[i].id;
            if (orderToSend[i].amount === 0) {
                orderToSend.splice(i, 1);
            }
        }

        const data = {
            client: {...client},
            orders: [...orderToSend]
        };

        dispatch(postClient(data));
    }

    let clientData = (
    <div className="ClientData" style={{display: show ? 'block' : 'none'}}>
        <input type="text" name="name" placeholder="Name" onChange={e => changeInput(e)} />
        <input type="number" name="phone" placeholder="Phone" onChange={e => changeInput(e)} />
        <input type="text" name="street" placeholder="Street" onChange={e => changeInput(e)} />
        <div>
            <button onClick={sendData}>Order</button>
            <button onClick={close}>Cancel</button>
        </div>
    </div>
    );

    if (loading) {
        clientData = <Spinner />
    }

    return (
        <>
            <Backdrop show={show} onClick={close}/>
            {clientData}
        </>
    );
};

export default ClientData;