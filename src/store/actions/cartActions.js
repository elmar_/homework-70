import axiosFetch from "../../axiosFetch";

export const ADD_DISH = 'ADD_DISH';
export const REMOVE_DISH = 'REMOVE_DISH';
export const FIRST_ADD = 'FIRST_ADD';

export const CLIENT_REQUEST = 'CLIENT_REQUEST';
export const CLIENT_SUCCESS = 'CLIENT_SUCCESS';
export const CLIENT_ERROR = 'CLIENT_ERROR';

export const addDish = id => ({type: ADD_DISH, id});
export const removeDish = id => ({type: REMOVE_DISH, id});
export const firstAdd = dish => ({type: FIRST_ADD, dish});

export const clientRequest = () => ({type: CLIENT_REQUEST});
export const clientSuccess = () => ({type: CLIENT_SUCCESS});
export const clientError = e => ({type: CLIENT_ERROR, e});

export const postClient = data => {
    return async dispatch => {
        try {
            dispatch(clientRequest());
            await axiosFetch.post('cart.json', data);
            dispatch(clientSuccess());
        } catch (e) {
            dispatch(clientError(e));
        }
    };
};

