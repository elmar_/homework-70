import {FETCH_MENU_ERROR, FETCH_MENU_REQUEST, FETCH_MENU_SUCCESS} from "../actions/menuActions";

const initialState = {
    loading: false,
    error: null,
    dishes: []
};

const menuReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MENU_REQUEST:
            return {...state, loading: true};
        case FETCH_MENU_SUCCESS:
            return {...state, loading: false, dishes: action.dishes};
        case FETCH_MENU_ERROR:
            return {...state, loading: false, error: action.error}
        default:
            return state;
    }
};

export default menuReducer;