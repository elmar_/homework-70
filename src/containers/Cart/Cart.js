import React, {useState} from 'react';
import './Cart.css';
import {useDispatch, useSelector} from "react-redux";
import Orders from "../../components/Orders/Orders";
import {removeDish} from "../../store/actions/cartActions";
import ClientData from "../ClientData/ClientData";

const Cart = () => {
    const dispatch = useDispatch();
    const {dishes, totalPrice, delivery} = useSelector(state => state.cart);
    const [show, setShow] = useState(false);

    const deleteOrder = id => {
        dispatch(removeDish(id));
    };

    const orders = Object.values(dishes).map(dish => {
            return <Orders key={dish.id} order={dish} removeOrder={deleteOrder} />
    });

    const closeModal = () => {
        setShow(false);
    } ;


    return (
        <div className="Cart">
            <ClientData show={show} close={closeModal} />
            <h4 className="cart-title">Orders</h4>
            <ul className="list">
                {orders}
            </ul>
            <div className="cart-bottom">
                <p><span>Досатавка:</span> <span><b>{delivery}</b></span></p>
                <p>Итого: <span><b>{totalPrice}</b></span></p>
            </div>
            <button className="cart-btn" onClick={() => setShow(!show)}>Place order</button>
        </div>
    );
};

export default Cart;