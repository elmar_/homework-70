import React from 'react';
import './Dish.css';

const Dish = ({dishData, addToCart}) => {
    return (
        <div className="Dish">
            <div className="img-block">
                <img src={dishData.image} alt="dish" />
            </div>
            <div className="info">
                <p><b>{dishData.dish}</b></p>
                <p>{dishData.price} KGS</p>
            </div>
            <button onClick={() => addToCart(dishData.id)}>Add to cart</button>
        </div>
    );
};

export default Dish;