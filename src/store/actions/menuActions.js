import axiosFetch from "../../axiosFetch";
import {firstAdd} from "./cartActions";

export const FETCH_MENU_REQUEST = 'FETCH_MENU_REQUEST';
export const FETCH_MENU_SUCCESS = 'FETCH_MENU_SUCCESS';
export const FETCH_MENU_ERROR = 'FETCH_MENU_ERROR';

export const fetchMenuRequest = () => ({type: FETCH_MENU_REQUEST});
export const fetchMenuSuccess = dishes => ({type: FETCH_MENU_SUCCESS, dishes});
export const fetchMenuError = error => ({type: FETCH_MENU_ERROR, error});

export const fetchDishes = () => {
    return async dispatch => {
        try {
            dispatch(fetchMenuRequest());
            const response = await axiosFetch.get('dishes.json');
            const values = Object.values(response.data);
            const cartValues = {};
            for (let i = 0; i < values.length; i ++) {
                cartValues[i + 1] = {
                    ...values[i],
                    amount: 0,
                    id: i + 1
                };
                delete cartValues[i + 1].image;

                values[i].id = i + 1;
            }
            dispatch(fetchMenuSuccess(values));
            dispatch(firstAdd(cartValues));
        } catch (e) {
            dispatch(fetchMenuError(e));
        }
    };
};