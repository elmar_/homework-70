import React from 'react';
import './Orders.css';

const Orders = ({order, removeOrder}) => {
    let display;
    if (order.amount === 0) {
        display = 'none';
    }
    return (
        <li className="order" onClick={() => removeOrder(order.id)} style={{display: display}}>
            <span>{order.dish} x{order.amount} </span> <span><b>{order.price * order.amount}</b></span>
        </li>
    );
};

export default Orders;